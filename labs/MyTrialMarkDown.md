# These are some examples of basic markdown formatting

# This is Heading 1
## This is Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

- This is a bullet
- This is another bullet

* This is a different way of doing bullets
* This is a bullet

1. This is an ordered numbered list
1. This is an ordered numbered list
1. This is an ordered numbered list

**This text is bold**

*This text is italic*

~~This is strikethrough~~

> This is a block quote

```json
This is a code block (json)
```

`This is inline code`

Below is a horizontal line

---

This is a [Link to the BBC home page](http://www.bbc.co.uk "Link to BBC home page")



